import 'package:flutter/material.dart';

// ignore: unexpected_token
void main() => runApp(new RotationIconApp());

class RotationIconApp extends StatelessWidget  {
  Widget build(BuildContext context)  {
    return new MaterialApp(
      title: 'Flutter Icon Rotation Demo',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(title: 'icon rotation'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var _position = 0.0;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
      ),
      body: new Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Transform.rotate(
              angle: _position *2*3.14,
              child: new Icon(Icons.android),
            ),
            new Slider(
              value: _position,
              onChanged: (var position1) {
                setState(() {
                  _position = position1;
                });
              }
            ),

            new Transform.rotate(
              angle: _position * -2*3.14,
              child: new Icon(Icons.android),
            ),
          ],
        )
      ),
    );
  }
}
